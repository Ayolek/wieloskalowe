package main;

import java.util.Random;

public class Neighbourhood {
	
	public static final String RANDOM = 		"RANDOM";
	public static final String MOORE = 			"MOORE";
	public static final String VONNEUMANN = 	"VONNEUMANN";
	public static final String LEFT = 			"LEFT";
	public static final String RIGHT = 			"RIGHT";
	public static final String TOP = 			"TOP";
	public static final String BOTTOM = 		"BOTTOM";
	public static final String HEXAGONAL_LEFT = "HEXAGONAL_LEFT";
	public static final String HEXAGONAL_RIGHT ="HEXAGONAL_RIGHT";
	
	private String selected;
	
	private Random r;
	
	public Neighbourhood(){
		this.selected = RANDOM;
	}
	
	public Neighbourhood(String name){
		this.selected = name;
	}
	
	public String getSelected(){
		return selected;
	}
	
	public void select(String neighbourhood){
		this.selected = neighbourhood;
	}
	
	public void selectRandom(){
		r=new Random();
		int position = r.nextInt(8);
		switch(position){
		case 0:
			select(Neighbourhood.MOORE);
			break;
		case 1:
			select(Neighbourhood.VONNEUMANN);
			break;
		case 2:
			select(Neighbourhood.LEFT);
			break;
		case 3:
			select(Neighbourhood.RIGHT);
			break;
		case 4:
			select(Neighbourhood.TOP);
			break;
		case 5:
			select(Neighbourhood.BOTTOM);
			break;
		case 6:
			select(Neighbourhood.HEXAGONAL_LEFT);
			break;
		case 7:
			select(Neighbourhood.HEXAGONAL_RIGHT);
			break;
		}
	}
}
