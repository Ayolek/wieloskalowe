package main;

public class Settings {
	
	private int windowHeightSize;
	private int windowWidthSize;
	private int cellSize;
	private int panelSize;
	private int tableSize;
	
	private double A;
	private double B;
	private double critical_value;
	private double time;
	private double dislocationGUN;
	

	public Settings(){
		setWindowHeightSize(660);
		setWindowWidthSize(860);
		setCellSize(1);
		setPanelSize(600);
		setTableSize();
		setA(86710969050178.5);
		setB(9.41268203527779);
		setCritical_value(11710667);
		setTime(0.065);
		setDislocationGUN((this.A/this.B)+(((1-(this.A/this.B))*Math.exp(-1*this.B*this.time))));
	}

	public double getA() {
		return A;
	}


	public double getB() {
		return B;
	}


	public double getCritical_value() {
		return critical_value;
	}


	public double getTime() {
		return time;
	}


	public double getDislocationGUN() {
		return dislocationGUN;
	}


	public void setA(double a) {
		A = a;
	}


	public void setB(double b) {
		B = b;
	}


	public void setCritical_value(double critical_value) {
		this.critical_value = critical_value;
	}


	public void setTime(double time) {
		this.time = time;
	}


	public void setDislocationGUN(double dislocationGUN) {
		this.dislocationGUN = dislocationGUN;
	}
	
	/**
	 * GETTERS	*/
	public int getPanelSize() {
		return panelSize;
	}

	public int getWindowHeightSize() {
		return windowHeightSize;
	}

	public int getWindowWidthSize() {
		return windowWidthSize;
	}

	public int getCellSize() {
		return cellSize;
	}
	
	public int getTableSize(){
		return tableSize;
	}

	/**
	 * SETTERS	*/
	public void setPanelSize(int panelSize) {
		this.panelSize = panelSize;
	}
	
	public void setWindowHeightSize(int windowHeightSize) {
		this.windowHeightSize = windowHeightSize;
	}

	public void setWindowWidthSize(int windowWidthSize) {
		this.windowWidthSize = windowWidthSize;
	}

	public void setCellSize(int cellSize) {
		this.cellSize = cellSize;
	}
	
	public void setTableSize(){
		tableSize = getPanelSize()/getCellSize();
	}

}
