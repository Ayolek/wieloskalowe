package main;

import java.awt.Color;
import java.util.Random;

public class Cell {
	
	private boolean isActive;
	private Color color;
	private int energy;
	private double dislocation;
	private boolean isRecrystalized;
	private boolean isOnBorder;
	
	private Random r = new Random();
	
	public Cell(){
		isActive = false;
		setRandomColor();
		dislocation = 0.0;
		isOnBorder = false;
		isRecrystalized = false;
		energy = 0;
	}

	public boolean isOnBorder() {
		return isOnBorder;
	}

	public void setOnBorder(boolean isOnBorder) {
		this.isOnBorder = isOnBorder;
	}

	public void setEnergy(int energy){
		this.energy = energy;
	}
	
	public int getEnergy(){
		return this.energy;
	}
	
	public boolean isActive() {
		return isActive;
	}

	public Color getColor() {
		return color;
	}

	public double getDislocation() {
		return dislocation;
	}

	public boolean isRecrystalized() {
		return isRecrystalized;
	}

	public void setDislocation(double dislocation) {
		this.dislocation += dislocation;
	}

	public void setRecrystalized(boolean isRecrystalized) {
		this.isRecrystalized = isRecrystalized;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	public void setRandomColor(){
		this.color =  new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255));
	}
	
	public void setSameAs(Cell cell){
		this.isActive = cell.isActive();
		this.color = cell.getColor();
		this.energy=cell.getEnergy();
		this.isRecrystalized=cell.isRecrystalized();
		this.isOnBorder=cell.isOnBorder();
		
		
	}
	
	public String toString(){
		if(isActive())
			return " "+1;
		else return " "+0;
	}
}
