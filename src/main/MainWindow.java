package main;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;

public class MainWindow extends JFrame implements MouseListener{
	
	//declarations
	private Neighbourhood mNeighbourhood;
	private Settings s;
	private MyPanel panel;
	private JPanel neighbourhoodPanel;
	private JPanel boundaryConditionPanel;
	private TextField input;
	private TextField inputPeriod;
	private JButton losuj;
	private JButton generate;
	private JButton clear;
	private JLabel info;
	private JLabel randomSeedInfo;
	private JLabel seedPeriodInfo;
	private JButton grow;
	private JLabel neighbourhoodInfo;
	private JLabel boundaryConditionsInfo;
	private JCheckBox radius;
	private JCheckBox everyStepSeeds;
	private TextField inputRadius;
	private TextField inputEveryStepSeeds;
	private TextField inputMCColors;
	private JButton monteCarlo;
	private JButton step;
	private JButton recrystalization;
	
	private ButtonGroup neighbourhoods;
	private JRadioButton randomButton;
	private JRadioButton mooreButton;
	private JRadioButton vonNeumannButton;
	private JRadioButton leftSideButton;
	private JRadioButton rightSideButton;
	private JRadioButton topSideButton;
	private JRadioButton bottomSideButton;
	private JRadioButton hexagonalLeftButton;
	private JRadioButton hexagonalRightButton;
	
	private ButtonGroup boundaryConditions;
	private JRadioButton periodicButton;
	private JRadioButton aperiodicButton;
	
	public MainWindow(Settings s){
		super("Seed Grow Application");
		
		this.s = s;

		generateView();
	
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(false);
		setBackground(Color.white);
		setSize(s.getWindowWidthSize(),s.getWindowHeightSize());		
	}
	
	private void generateView(){
		
		//initialization
		panel = new MyPanel(s);
		neighbourhoodPanel = new JPanel();
		boundaryConditionPanel = new JPanel();
		losuj = new JButton("set seeds");
		generate = new JButton("set seeds");
		info = new JLabel("Welcome!");
		input = new TextField();
		inputPeriod = new TextField();
		randomSeedInfo = new JLabel("Enter amount of seeds below:");
		seedPeriodInfo = new JLabel("Enter seeds period below:");
		clear = new JButton("clear");
		grow = new JButton("grow step");
		neighbourhoods = new ButtonGroup();
		neighbourhoodInfo = new JLabel("Select neighbourhood:");
		randomButton = new JRadioButton("Random");
		mooreButton = new JRadioButton("Moore");
		vonNeumannButton = new JRadioButton("Von Neumann");
		leftSideButton = new JRadioButton("Left side");
		rightSideButton = new JRadioButton("Right side");
		topSideButton = new JRadioButton("Top side");
		bottomSideButton = new JRadioButton("Bottom side");
		hexagonalLeftButton = new JRadioButton("Hexagonal left");
		hexagonalRightButton = new JRadioButton("Hexagonal right");
		mNeighbourhood = new Neighbourhood();
		boundaryConditions = new ButtonGroup();
		periodicButton = new JRadioButton("Periodic");
		aperiodicButton = new JRadioButton("Aperiodic");
		boundaryConditionsInfo = new JLabel("Select boundary condition:");
		radius = new JCheckBox("set radius:");
		everyStepSeeds = new JCheckBox("seeds every step:");
		inputRadius = new TextField("1");
		inputEveryStepSeeds = new TextField("1");
		inputMCColors = new TextField("2");
		monteCarlo = new JButton("Monte Carlo");
		step = new JButton("step");
		recrystalization = new JButton("Recrystalize");
		
		//bounds
		losuj.setBounds(680, 80, 150, 30);
		generate.setBounds(680, 155, 150, 30);
		info.setBounds(640, 3, 150, 60);		
		input.setBounds(640, 85, 30, 20);
		inputPeriod.setBounds(640, 160, 30, 20);
		inputRadius.setBounds(725, 115, 30, 20);
		randomSeedInfo.setBounds(640, 65, 180, 10);
		seedPeriodInfo.setBounds(640, 135, 150, 20);
		clear.setBounds(660, 585, 150, 30);
		grow.setBounds(660, 550, 150, 30);
		recrystalization.setBounds(660, 515, 150, 30);
		radius.setBounds(640, 115, 100, 20);
		inputMCColors.setBounds(640,475,30,20);
		monteCarlo.setBounds(675, 470, 150, 15);
		step.setBounds(675, 485, 150, 15);
		
		
		neighbourhoodPanel.setBounds(640, 205, 200, 200);
		neighbourhoodInfo.setBounds(0, 0, 150, 20);
		neighbourhoodPanel.setLayout(null);
		randomButton.setBounds(10, 20, 150, 20);
		mooreButton.setBounds(10, 40, 150, 20);
		vonNeumannButton.setBounds(10, 60, 150, 20);
		leftSideButton.setBounds(10, 80, 150, 20);
		rightSideButton.setBounds(10, 100, 150, 20);
		topSideButton.setBounds(10, 120, 150, 20);
		bottomSideButton.setBounds(10, 140, 150, 20);
		hexagonalLeftButton.setBounds(10, 160, 150, 20);
		hexagonalRightButton.setBounds(10, 180, 150, 20);
		
		boundaryConditionPanel.setLayout(null);
		boundaryConditionPanel.setBounds(640, 405, 200, 60);
		boundaryConditionsInfo.setBounds(0, 0, 200, 20);
		periodicButton.setBounds(10, 20, 190, 20);
		aperiodicButton.setBounds(10, 40, 190, 20);		
		
		everyStepSeeds.setBounds(640, 185, 130, 20);
		inputEveryStepSeeds.setBounds(770, 186, 30, 20);
		
		//extras
		input.setText("1");
		inputPeriod.setText("1");
		
		losuj.addMouseListener(this);
		generate.addMouseListener(this);
		clear.addMouseListener(this);
		grow.addMouseListener(this);
		monteCarlo.addMouseListener(this);
		step.addMouseListener(this);
		recrystalization.addMouseListener(this);
		
		randomButton.setSelected(true);
		randomButton.addActionListener(neighbourhoodListener);
		randomButton.setActionCommand(Neighbourhood.RANDOM);
		mooreButton.addActionListener(neighbourhoodListener);
		mooreButton.setActionCommand(Neighbourhood.MOORE);
		vonNeumannButton.addActionListener(neighbourhoodListener);
		vonNeumannButton.setActionCommand(Neighbourhood.VONNEUMANN);
		leftSideButton.addActionListener(neighbourhoodListener);
		leftSideButton.setActionCommand(Neighbourhood.LEFT);
		rightSideButton.addActionListener(neighbourhoodListener);
		rightSideButton.setActionCommand(Neighbourhood.RIGHT);
		topSideButton.addActionListener(neighbourhoodListener);
		topSideButton.setActionCommand(Neighbourhood.TOP);
		bottomSideButton.addActionListener(neighbourhoodListener);
		bottomSideButton.setActionCommand(Neighbourhood.BOTTOM);
		hexagonalLeftButton.addActionListener(neighbourhoodListener);
		hexagonalLeftButton.setActionCommand(Neighbourhood.HEXAGONAL_LEFT);
		hexagonalRightButton.addActionListener(neighbourhoodListener);
		hexagonalRightButton.setActionCommand(Neighbourhood.HEXAGONAL_RIGHT);
		
		periodicButton.setSelected(true);
		periodicButton.addActionListener(boundaryConditionsListener);
		periodicButton.setActionCommand(BoundaryCondition.PERIODIC);
		aperiodicButton.addActionListener(boundaryConditionsListener);
		aperiodicButton.setActionCommand(BoundaryCondition.APERIODIC);
		
		neighbourhoods.add(randomButton);
		neighbourhoods.add(mooreButton);
		neighbourhoods.add(vonNeumannButton);
		neighbourhoods.add(leftSideButton);
		neighbourhoods.add(rightSideButton);
		neighbourhoods.add(topSideButton);
		neighbourhoods.add(bottomSideButton);
		neighbourhoods.add(hexagonalLeftButton);
		neighbourhoods.add(hexagonalRightButton);
		
		neighbourhoodPanel.add(neighbourhoodInfo);
		neighbourhoodPanel.add(randomButton);
		neighbourhoodPanel.add(mooreButton);
		neighbourhoodPanel.add(vonNeumannButton);
		neighbourhoodPanel.add(leftSideButton);
		neighbourhoodPanel.add(rightSideButton);
		neighbourhoodPanel.add(topSideButton);
		neighbourhoodPanel.add(bottomSideButton);
		neighbourhoodPanel.add(hexagonalLeftButton);
		neighbourhoodPanel.add(hexagonalRightButton);
		
		boundaryConditions.add(periodicButton);
		boundaryConditions.add(aperiodicButton);
		
		boundaryConditionPanel.add(boundaryConditionsInfo);
		boundaryConditionPanel.add(periodicButton);
		boundaryConditionPanel.add(aperiodicButton);

		//composition
		add(panel);
		add(neighbourhoodPanel);
		add(boundaryConditionPanel);
		add(losuj);
		add(generate);
		add(info);
		add(input);
		add(inputPeriod);
		add(inputRadius);
		add(randomSeedInfo);
		add(seedPeriodInfo);
		add(clear);
		add(grow);
		add(radius);
		add(everyStepSeeds);
		add(inputEveryStepSeeds);
		add(inputMCColors);
		add(monteCarlo);
		add(step);
		add(recrystalization);

	}
	
	private void randomSeed(String sAmountOfSeeds){
		String result="";
		int amountOfSeeds = 1;
		
		if(sAmountOfSeeds.length()>0)
			amountOfSeeds = Integer.parseInt(sAmountOfSeeds);
		int amount = 1;
		
		if((amountOfSeeds>0) && (amountOfSeeds<s.getTableSize()*s.getTableSize())) 
			amount = amountOfSeeds;
		
		if(amountOfSeeds>(s.getTableSize()*s.getTableSize()))
			amount = (s.getTableSize()*s.getTableSize());
		
		if(radius.isSelected()){
			for(int i=0; i<amount; i++){
				result = panel.randomSeed(inputRadius.getText());
			}
		}else{
			for(int i=0; i<amount; i++){
				result = panel.randomSeed();
			}
		}
		
		info.setText(result);
	}
	
	private void clear(){
		panel.clear();
		info.setText("Table cleared.");
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		if(e.getSource().equals(losuj)){
				randomSeed(input.getText());
		}
		
		if(e.getSource().equals(clear)){
			clear();
		}
		
		if(e.getSource().equals(step)){
				panel.step();
		}
		
		if(e.getSource().equals(recrystalization)){
			panel.recrystalize();
		}
		
		if(e.getSource().equals(grow)){
			if(everyStepSeeds.isSelected()){
				randomSeed(inputEveryStepSeeds.getText());
			}
			panel.growSeeds(mNeighbourhood);
		}
		
		if(e.getSource().equals(monteCarlo)){
			panel.monteCarlo(inputMCColors.getText());
		}
		
		if(e.getSource().equals(generate)){
			info.setText(panel.regularSet(inputPeriod.getText()));
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}
	
	private ActionListener neighbourhoodListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			mNeighbourhood.select(e.getActionCommand());
		}
	};
	
	private ActionListener boundaryConditionsListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			panel.getBoundaryCondition().setBoundaryCondition(e.getActionCommand());
		}
	};
}
