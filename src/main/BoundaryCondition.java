package main;

public class BoundaryCondition {
	
	public static final String PERIODIC= "PERIODIC";
	public static final String APERIODIC = "APERIODIC";
	
	private String selected;
	
	public BoundaryCondition(){
		this.selected = PERIODIC;
	}
	
	public void setBoundaryCondition(String condition){
		this.selected = condition;
	}
	
	public String getBoundaryCondition(){
		return selected;
	}
}
