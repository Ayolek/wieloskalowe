package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;

public class MyPanel extends JPanel implements MouseListener {

	private Settings s;
	private Random r = new Random();

	private BoundaryCondition boundaryCondition;
	ArrayList<Point> points;
	private Cell[][] cells;
	private Cell[][] cellsTemporary;
	private int step = 0;
	private boolean canStart = true;

	private ArrayList<Color> monteCarloColors;

	public MyPanel(Settings s) {
		this.s = s;
		cells = new Cell[s.getTableSize()][s.getTableSize()];
		cellsTemporary = new Cell[s.getTableSize()][s.getTableSize()];
		boundaryCondition = new BoundaryCondition();
		monteCarloColors = new ArrayList<Color>();
		points = new ArrayList<MyPanel.Point>();

		for (int i = 0; i < s.getTableSize(); i++)
			for (int j = 0; j < s.getTableSize(); j++) {
				cells[i][j] = new Cell();
				cellsTemporary[i][j] = new Cell();
			}
		updateCells();
		setBounds(15, 15, s.getPanelSize() + 1, s.getPanelSize() + 1);
		setBackground(Color.white);
		this.addMouseListener(this);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D graphics2d = (Graphics2D) g;
		graphics2d.drawRect(0, 0, s.getPanelSize(), s.getPanelSize());
		for (int i = 0; i < s.getTableSize(); i++)
			for (int j = 0; j < s.getTableSize(); j++) {
				if (cells[i][j].isActive()) {
					graphics2d.setColor(cells[i][j].getColor());
					graphics2d.fillRect(i * s.getCellSize(),
							j * s.getCellSize(), s.getCellSize(),
							s.getCellSize());
				}
			}
	}

	public BoundaryCondition getBoundaryCondition() {
		return boundaryCondition;
	}

	private void drawCell(MouseEvent e) {
		cells[getPositionInTable(e.getX())][getPositionInTable(e.getY())]
				.setActive(true);
		cellsTemporary[getPositionInTable(e.getX())][getPositionInTable(e
				.getY())].setActive(true);
	}

	private int getPositionInTable(int mousePosition) {
		return (int) mousePosition / s.getCellSize();
	}

	public String regularSet(String sPeriod) {
		int period = -1;
		if (sPeriod.length() > 0)
			period = Integer.parseInt(sPeriod);
		if (period == 0)
			period = 1;

		if (period != -1) {
			for (int i = 0; i < s.getTableSize(); i += period)
				for (int j = 0; j < s.getTableSize(); j += period) {
					if (!cells[i][j].isActive()) {
						cells[i][j].setActive(true);
						cells[i][j].setRandomColor();
						cellsTemporary[i][j].setSameAs(cells[i][j]);
					}
				}
			repaint();
			return "Seeds set successfuly.";
		} else {
			return "Wrong period.";
		}
	}

	public String randomSeed() {

		int count = 0;
		int x, y;
		boolean success = false;

		do {
			x = r.nextInt(s.getTableSize());
			y = r.nextInt(s.getTableSize());
			if (!cells[x][y].isActive()) {
				cells[x][y].setActive(true);
				cells[x][y].setRandomColor();
				cellsTemporary[x][y].setSameAs(cells[x][y]);
				success = true;
			}
			count++;
		} while (!success && count < 10);

		if (!success) {
			return "<html>Too hard for me<br>to random seed.<br>Try it Yourself.";
		} else {
			repaint();
			return "Seeds set successfuly.";
		}
	}

	public String randomSeed(String sRadius) {

		int iRadius;
		int count = 0;
		int x, y;
		boolean success;

		if (sRadius.length() > 0) {
			iRadius = Integer.parseInt(sRadius);
		} else {
			iRadius = 1;
		}

		do {
			success = true;
			x = r.nextInt(s.getTableSize());
			y = r.nextInt(s.getTableSize());

			for (int i = x - iRadius; i < (x + iRadius); i++) {
				if (success)
					for (int j = y - iRadius; j < (y + iRadius); j++) {
						if (i >= 0 && j >= 0 && i < s.getTableSize()
								&& j < s.getTableSize()
								&& inRange(x, y, i, j, iRadius))
							if (cells[i][j].isActive()) {
								success = false;
								break;
							}
					}
				else
					break;
			}

			if (success) {
				cells[x][y].setActive(true);
				cells[x][y].setRandomColor();
				cellsTemporary[x][y].setSameAs(cells[x][y]);
			}
			count++;
		} while (!success && count < 10);

		if (!success) {
			return "<html>Another seed in range near: <br>(" + x + ":" + y
					+ ")";
		} else {
			repaint();
			return "Seed set successfuly.";
		}
	}

	private boolean inRange(int x, int y, int i, int j, int range) {
		double distance = Math.sqrt(((x - i) * (x - i)) * ((y - j) * (y - j)));
		if (distance <= range)
			return true;
		else
			return false;
	}

	public void clear() {
		for (int i = 0; i < s.getTableSize(); i++)
			for (int j = 0; j < s.getTableSize(); j++) {
				cells[i][j].setActive(false);
				cellsTemporary[i][j].setActive(false);
				cells[i][j].setRandomColor();
				cellsTemporary[i][j].setRandomColor();
				cellsTemporary[i][j].setDislocation(0.0);
				cellsTemporary[i][j].setRecrystalized(false);
				cellsTemporary[i][j].setOnBorder(false);
			}
		updateCells();
		repaint();
	}

	public void growSeeds(Neighbourhood neighbourhood) {
		Cell result = new Cell();

		int min;
		int max;

		if (boundaryCondition.getBoundaryCondition().equals(
				BoundaryCondition.PERIODIC)) {
			min = 0;
			max = s.getTableSize();
		} else {
			min = 1;
			max = s.getTableSize() - 1;
		}

		for (int i = min; i < max; i++) {
			for (int j = min; j < max; j++) {			
				if (!cells[i][j].isActive()||cells[i][j].isRecrystalized()) {
					result = checkNeighbourhood(i, j, neighbourhood, result);
					if (result.isActive() && result != null) {
						cellsTemporary[i][j].setSameAs(result);
					}
				}
			}
		}
		updateCells();
	}

	private void showTable() {
		for (int i = 0; i < s.getTableSize(); i++) {
			for (int j = 0; j < s.getTableSize(); j++) {
				System.out.print(cells[j][i].toString());
			}
			System.out.print("\n");
		}
		System.out.print("\n");
	}

	private void showTempTable() {
		for (int i = 0; i < s.getTableSize(); i++) {
			for (int j = 0; j < s.getTableSize(); j++) {
				System.out.print(cellsTemporary[j][i].toString());
			}
			System.out.print("\n");
		}
		System.out.print("\n");
	}

	private void updateCells() {
		for (int i = 0; i < s.getTableSize(); i++)
			for (int j = 0; j < s.getTableSize(); j++)
				cells[i][j].setSameAs(cellsTemporary[i][j]);
		repaint();
	}

	public void show() {
		showTable();
		showTempTable();
	}
	
	public void recrystalize(){
		//TODO
		
		canStart = true;
		if(step==0){
		for (int i = 0; i < s.getTableSize(); i++)
			for (int j = 0; j < s.getTableSize(); j++){
				if(!cells[i][j].isActive()){
					canStart = false;				
				}
			}
		}
		
		if(canStart){
			step++;
			if(step==1){
				setBorders();
			}
			
			double dislocationPackage = s.getDislocationGUN();
			double leftDislocations = dislocationPackage;
			double size = s.getTableSize()*s.getTableSize();
			double simplePackage = dislocationPackage/size;
			for (int i = 0; i < s.getTableSize(); i++)
				for (int j = 0; j < s.getTableSize(); j++){
					if(cells[i][j].isOnBorder()&& !cells[i][j].isRecrystalized()){
						double value = simplePackage*r.nextDouble()*0.9;
						cells[i][j].setDislocation(value);
						leftDislocations-=value;
					}else if(!cells[i][j].isRecrystalized()){
						double value = simplePackage*r.nextDouble()*0.1;
						cells[i][j].setDislocation(value);
						leftDislocations-=value;
					}
				}
			
			for (int i = 0; i < s.getTableSize(); i++)
				for (int j = 0; j < s.getTableSize(); j++){
					if(cells[i][j].getDislocation()>=s.getCritical_value()){
						cells[i][j].setRecrystalized(true);
						cells[i][j].setRandomColor();
						cellsTemporary[i][j].setSameAs(cells[i][j]);
					}
				}
			//System.out.print(cells[50][50].getDislocation()+" / "+s.getCritical_value()+"\n");
			growCrystalized();
			
		}
	}
	
	private void growCrystalized(){
		for (int i = 0; i < s.getTableSize(); i++)
			for (int j = 0; j < s.getTableSize(); j++){
				if(!cells[i][j].isRecrystalized()){
						ArrayList<Color> colors = new ArrayList<Color>();
						int count = 1;
						for (int x = i - 1; x < i + 2; x++)
							for (int y = j - 1; y < j + 2; y++) {
								if ((count % 4 != 1)
										&& (cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
												.getTableSize()) % s.getTableSize()].isRecrystalized()))
									colors.add(cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
											.getTableSize()) % s.getTableSize()].getColor());
								count++;
							}
						if(colors.size()>0){
							cellsTemporary[i][j].setColor(colors.get(r.nextInt(colors.size())));
							cellsTemporary[i][j].setRecrystalized(true);
						}
				}
			}
		updateCells();
	}

	
	private void setBorders(){
		
		for (int i = 0; i < s.getTableSize(); i++)
			for (int j = 0; j < s.getTableSize(); j++){
				
				int xMIN=i-1;
				int yMIN=j-1;
				int xMAX=i+2;
				int yMAX=j+2;
				
				if(i==0){xMIN=i;}
				if(j==0){yMIN=j;}
				if(i==s.getTableSize()-1){xMAX=i+1;}
				if(j==s.getTableSize()-1){yMAX=j+1;}
				
				
				for (int x = xMIN; x < xMAX; x++)
					for (int y = yMIN; y < yMAX; y++) {
						if(!cells[x][y].getColor().equals(cells[i][j].getColor())){
							cells[i][j].setOnBorder(true);
						}
					}
		}
	}
	
	private void showBorders(){
		System.out.print("\n");
		for (int i = 0; i < s.getTableSize(); i++){
			for (int j = 0; j < s.getTableSize(); j++){
				System.out.print(cells[i][j].isOnBorder()+" ");
			}
			System.out.print("\n");
		}
		System.out.print("--------------------");
	}

	public void monteCarlo(String number_of_colors) {
		// prepare list with colors
		int colors_size;
		colors_size = Integer.parseInt(number_of_colors);

		if (colors_size < 2) {
			colors_size = 2;
		}

		monteCarloColors.clear();

		for (int i = 0; i < colors_size; i++) {
			monteCarloColors.add(new Color(r.nextInt(255), r.nextInt(255), r
					.nextInt(255)));
		}

		// prepare painting table
		clear();

		for (int i = 0; i < s.getTableSize(); i++)
			for (int j = 0; j < s.getTableSize(); j++) {
				cells[i][j].setActive(true);
				cells[i][j].setColor(monteCarloColors.get(r
						.nextInt(monteCarloColors.size())));
			}
		repaint();

		// calculate energy and add all cells to the list	
		for (int i = 0; i < s.getTableSize(); i++)
			for (int j = 0; j < s.getTableSize(); j++) {
				cells[i][j].setEnergy(countEnergy(i, j));
			}	
		
		addAsPoints();
		showEnergy();
	}
	
	public void step(){
		
			addAsPoints();
			while(!points.isEmpty()){
				int position = r.nextInt(points.size());
				Point point = points.get(position);
				checkEnergy(point);
				points.remove(position);
			}	
			showEnergy();	
		repaint();
	}
	private void addAsPoints(){
		points.clear();
		for (int i = 0; i < s.getTableSize(); i++)
			for (int j = 0; j < s.getTableSize(); j++)
				points.add(new Point(i,j));
	}
	
	private void showEnergy(){
		System.out.print("\n");
		for (int i = 0; i < s.getTableSize(); i++){
			for (int j = 0; j < s.getTableSize(); j++){
				System.out.print(cells[i][j].getEnergy()+" ");
			}
			System.out.print("\n");
		}
		System.out.print("--------------------");
	}
	
	private void checkEnergy(Point point){
		int x = point.getX();
		int y = point.getY();
		
		Color color = cells[x][y].getColor();
		Color tempColor = monteCarloColors.get(r.nextInt(monteCarloColors.size()));
		cells[x][y].setColor(tempColor);
		if((countEnergy(x, y)>cells[x][y].getEnergy())){
			cells[x][y].setColor(color);
		}
		else{
			cells[x][y].setEnergy(countEnergy(x, y));
		}
	}

	private int countEnergy(int i, int j) {
		int energy = 0;
		int xMIN=i-1;
		int yMIN=j-1;
		int xMAX=i+2;
		int yMAX=j+2;
		
		if(i==0){xMIN=i;}
		if(j==0){yMIN=j;}
		if(i==s.getTableSize()-1){xMAX=i+1;}
		if(j==s.getTableSize()-1){yMAX=j+1;}
		
		
		for (int x = xMIN; x < xMAX; x++)
			for (int y = yMIN; y < yMAX; y++) {
				if(!cells[x][y].getColor().equals(cells[i][j].getColor())){
					energy++;
				}
			}
		return energy;
	}

	private Cell checkNeighbourhood(int i, int j, Neighbourhood neighbourhood,
			Cell resultCell) {

		Neighbourhood tempNeighbourhood = new Neighbourhood();

		if (neighbourhood.getSelected().equals(Neighbourhood.RANDOM)) {
			tempNeighbourhood.selectRandom();
		} else {
			tempNeighbourhood.select(neighbourhood.getSelected());
		}

		switch (tempNeighbourhood.getSelected()) {
		case Neighbourhood.MOORE:
			resultCell = moore(i, j, resultCell);
			break;
		case Neighbourhood.VONNEUMANN:
			resultCell = vonNeumann(i, j, resultCell);
			break;
		case Neighbourhood.LEFT:
			resultCell = left(i, j, resultCell);
			break;
		case Neighbourhood.RIGHT:
			resultCell = right(i, j, resultCell);
			break;
		case Neighbourhood.TOP:
			resultCell = top(i, j, resultCell);
			break;
		case Neighbourhood.BOTTOM:
			resultCell = bottom(i, j, resultCell);
			break;
		case Neighbourhood.HEXAGONAL_LEFT:
			resultCell = hexagonalLeft(i, j, resultCell);
			break;
		case Neighbourhood.HEXAGONAL_RIGHT:
			resultCell = hexagonalRight(i, j, resultCell);
			break;
		default:
			resultCell = null;
		}
		return resultCell;
	}

	private Cell setColor(ArrayList<Color> colors, Cell resultCell) {

		if (colors.size() > 0) {
			resultCell.setColor(colors.get(r.nextInt(colors.size())));
			resultCell.setActive(true);
			colors.clear();
			return resultCell;
		} else {
			Cell emptyCell = new Cell();
			emptyCell.setActive(false);
			emptyCell.setColor(Color.white);
			return emptyCell;
		}
	}

	private Cell moore(int i, int j, Cell resultCell) {
		ArrayList<Color> colors = new ArrayList<Color>();

		for (int x = i - 1; x < i + 2; x++)
			for (int y = j - 1; y < j + 2; y++) {
				if (cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
						.getTableSize()) % s.getTableSize()].isActive())
					colors.add(cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
							.getTableSize()) % s.getTableSize()].getColor());
			}

		resultCell = setColor(colors, resultCell);
		return resultCell;
	}

	private Cell vonNeumann(int i, int j, Cell resultCell) {
		ArrayList<Color> colors = new ArrayList<Color>();

		for (int y = j - 1; y < j + 2; y++) {
			if (cells[(i + s.getTableSize()) % s.getTableSize()][(y + s
					.getTableSize()) % s.getTableSize()].isActive())
				colors.add(cells[(i + s.getTableSize()) % s.getTableSize()][(y + s
						.getTableSize()) % s.getTableSize()].getColor());
		}

		for (int x = i - 1; x < i + 2; x++) {
			if (cells[(x + s.getTableSize()) % s.getTableSize()][(j + s
					.getTableSize()) % s.getTableSize()].isActive())
				colors.add(cells[(x + s.getTableSize()) % s.getTableSize()][(j + s
						.getTableSize()) % s.getTableSize()].getColor());
		}

		resultCell = setColor(colors, resultCell);
		return resultCell;
	}

	private Cell left(int i, int j, Cell resultCell) {
		ArrayList<Color> colors = new ArrayList<Color>();

		for (int x = i - 1; x < i + 1; x++)
			for (int y = j - 1; y < j + 2; y++) {
				if (cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
						.getTableSize()) % s.getTableSize()].isActive())
					colors.add(cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
							.getTableSize()) % s.getTableSize()].getColor());
			}

		resultCell = setColor(colors, resultCell);
		return resultCell;
	}

	private Cell right(int i, int j, Cell resultCell) {
		ArrayList<Color> colors = new ArrayList<Color>();

		for (int x = i; x < i + 2; x++)
			for (int y = j - 1; y < j + 2; y++) {
				if (cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
						.getTableSize()) % s.getTableSize()].isActive())
					colors.add(cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
							.getTableSize()) % s.getTableSize()].getColor());
			}

		resultCell = setColor(colors, resultCell);
		return resultCell;
	}

	private Cell top(int i, int j, Cell resultCell) {
		ArrayList<Color> colors = new ArrayList<Color>();

		for (int x = i - 1; x < i + 2; x++)
			for (int y = j - 1; y < j + 1; y++) {
				if (cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
						.getTableSize()) % s.getTableSize()].isActive())
					colors.add(cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
							.getTableSize()) % s.getTableSize()].getColor());
			}

		resultCell = setColor(colors, resultCell);
		return resultCell;
	}

	private Cell bottom(int i, int j, Cell resultCell) {
		ArrayList<Color> colors = new ArrayList<Color>();

		for (int x = i - 1; x < i + 2; x++)
			for (int y = j; y < j + 2; y++) {
				if (cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
						.getTableSize()) % s.getTableSize()].isActive())
					colors.add(cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
							.getTableSize()) % s.getTableSize()].getColor());
			}

		resultCell = setColor(colors, resultCell);
		return resultCell;
	}

	private Cell hexagonalLeft(int i, int j, Cell resultCell) {
		ArrayList<Color> colors = new ArrayList<Color>();
		int count = 1;
		for (int x = i - 1; x < i + 2; x++)
			for (int y = j - 1; y < j + 2; y++) {
				if ((count % 4 != 3)
						&& (cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
								.getTableSize()) % s.getTableSize()].isActive()))
					colors.add(cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
							.getTableSize()) % s.getTableSize()].getColor());
				count++;
			}

		resultCell = setColor(colors, resultCell);
		return resultCell;
	}

	private Cell hexagonalRight(int i, int j, Cell resultCell) {
		ArrayList<Color> colors = new ArrayList<Color>();
		int count = 1;
		for (int x = i - 1; x < i + 2; x++)
			for (int y = j - 1; y < j + 2; y++) {
				if ((count % 4 != 1)
						&& (cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
								.getTableSize()) % s.getTableSize()].isActive()))
					colors.add(cells[(x + s.getTableSize()) % s.getTableSize()][(y + s
							.getTableSize()) % s.getTableSize()].getColor());
				count++;
			}

		resultCell = setColor(colors, resultCell);
		return resultCell;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		drawCell(e);
		repaint();
	}
	
	private class Point{
		private int x;
		private int y;
		
		public Point(int x, int y){
			this.x = x;
			this.y = y;
		}
		
		public int getX(){
			return this.x;
		}
		
		public int getY(){
			return this.y;
		}
	}
}
